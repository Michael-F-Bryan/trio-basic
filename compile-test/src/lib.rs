extern crate failure;
extern crate glob;
#[macro_use]
extern crate slog;
extern crate hir;
extern crate runtime;
extern crate syntax;

mod discovery;
mod runner;
mod syntax_pass;

pub use discovery::Tests;
pub use runner::TestRunner;
use std::fmt::{self, Display, Formatter};
pub use syntax_pass::SyntaxPass;

use failure::Error;
use slog::Logger;

pub trait Test {
    fn run(&self, logger: &Logger) -> TestOutcome;
    fn name(&self) -> &str;
}

#[derive(Debug)]
pub enum TestOutcome {
    Pass,
    SetupFailed(Error),
    GenericFailure(Error),
}

impl TestOutcome {
    pub fn is_ok(&self) -> bool {
        match *self {
            TestOutcome::Pass => true,
            _ => false,
        }
    }
}

impl Display for TestOutcome {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            TestOutcome::Pass => write!(f, "Pass"),
            TestOutcome::GenericFailure(ref e) => e.fmt(f),
            TestOutcome::SetupFailed(ref e) => write!(f, "Setup failed: {}", e),
        }
    }
}
