use super::{Test, TestOutcome};
use failure::Fail;
use slog::Logger;
use std::fmt::{self, Display, Formatter};
use std::fs;
use std::path::PathBuf;
use syntax;

#[derive(Debug, Clone, PartialEq)]
pub struct SyntaxPass {
    pub(crate) filename: PathBuf,
}

impl SyntaxPass {
    pub fn new<P: Into<PathBuf>>(filename: P) -> SyntaxPass {
        SyntaxPass {
            filename: filename.into(),
        }
    }
}

impl Test for SyntaxPass {
    fn run(&self, logger: &Logger) -> TestOutcome {
        let logger = logger.new(o!("syntax-pass" => self.name().to_string()));
        debug!(logger, "Starting test");

        let src = match fs::read_to_string(&self.filename) {
            Ok(s) => s,
            Err(e) => return TestOutcome::SetupFailed(e.context("Unable to read the file").into()),
        };

        match syntax::parse(&src) {
            Ok(_) => {
                debug!(logger, "Test Passed");
                TestOutcome::Pass
            }
            Err(e) => {
                debug!(logger, "Test Failed"; 
                    "error" => e.to_string(),
                    "full-error" => format_args!("{:?}", e),
                    "src" => format_args!("{:?}", src));

                TestOutcome::GenericFailure(e.into())
            }
        }
    }

    fn name(&self) -> &str {
        self.filename
            .file_stem()
            .expect("All files have a name")
            .to_str()
            .expect("Filenames are UTF8")
    }
}

impl Display for SyntaxPass {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        self.name().fmt(f)
    }
}
