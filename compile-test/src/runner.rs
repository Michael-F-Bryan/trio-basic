use super::{Test, TestOutcome};
use discovery::Tests;
use slog::Logger;
use syntax_pass::SyntaxPass;

#[derive(Debug)]
pub struct TestRunner {
    logger: Logger,
}

impl TestRunner {
    pub fn new(logger: &Logger) -> TestRunner {
        TestRunner {
            logger: logger.clone(),
        }
    }

    pub fn execute_test(&self, tests: &Tests) -> TestResults {
        let mut results = TestResults::default();

        if !tests.syntax_pass.is_empty() {
            let (pass, fail) = self.syntax_pass(&tests.syntax_pass);
            results.syntax_pass_pass = pass;
            results.syntax_pass_fail = fail;
        }

        results
    }

    fn syntax_pass(
        &self,
        tests: &[SyntaxPass],
    ) -> (Vec<SyntaxPass>, Vec<(SyntaxPass, TestOutcome)>) {
        info!(self.logger, "Executing syntax-pass tests";
                "amount" => tests.len());

        let outcomes: Vec<_> = tests
            .iter()
            .map(|test| (test.clone(), test.run(&self.logger)))
            .collect();

        let mut pass = Vec::new();
        let mut fail = Vec::new();

        for (test, outcome) in outcomes {
            if outcome.is_ok() {
                pass.push(test);
            } else {
                fail.push((test, outcome));
            }
        }

        (pass, fail)
    }
}

#[derive(Debug, Default)]
pub struct TestResults {
    pub syntax_pass_pass: Vec<SyntaxPass>,
    pub syntax_pass_fail: Vec<(SyntaxPass, TestOutcome)>,
}

impl TestResults {
    pub fn is_ok(&self) -> bool {
        self.syntax_pass_fail.is_empty()
    }
}
