use failure::{Error, ResultExt};
use glob::Pattern;
use std::path::{Path, PathBuf};
use syntax_pass::SyntaxPass;
use Test;

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Tests {
    pub syntax_pass: Vec<SyntaxPass>,
    pub ignored: Vec<PathBuf>,
}

impl Tests {
    pub fn new() -> Tests {
        Default::default()
    }

    pub fn for_directory<P: AsRef<Path>>(test_root: P) -> Result<Self, Error> {
        let mut tests = Tests::new();
        tests.discover(test_root.as_ref())?;
        Ok(tests)
    }

    fn discover(&mut self, test_root: &Path) -> Result<(), Error> {
        self.find_syntax_pass(test_root.join("syntax-pass"))?;

        Ok(())
    }

    fn find_syntax_pass(&mut self, test_directory: PathBuf) -> Result<(), Error> {
        let pattern = Pattern::new("*.bas").expect("Always valid");

        for entry in test_directory
            .read_dir()
            .context("Unable to read the syntax-pass directory")?
        {
            let entry = entry.context("Couldn't inspect the file")?;
            let filename = entry.path();

            if filename.is_file() && pattern.matches_path(&filename) {
                let test = SyntaxPass::new(filename);

                if test.name().starts_with("_") {
                    self.ignored.push(test.filename);
                } else {
                    self.syntax_pass.push(test);
                }
            }
        }

        Ok(())
    }
}
