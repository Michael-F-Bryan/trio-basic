extern crate compile_test;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;

use compile_test::{Test, TestRunner, Tests};
use slog::{Drain, Logger};
use std::path::Path;
use std::process;

fn main() {
    process::exit(run());
}

fn run() -> i32 {
    let test_dir = Path::new(env!("CARGO_MANIFEST_DIR")).join("tests");
    let root_logger = initialize_logging();

    let logger = root_logger.new(o!("phase" => "setup"));
    info!(logger, "Started integration tests");

    let tests = match Tests::for_directory(&test_dir) {
        Ok(t) => t,
        Err(e) => {
            error!(logger, "Test discovery failed!";
                "error" => e.to_string());
            return 1;
        }
    };
    info!(logger, "Finished test discovery";
        "syntax-pass" => tests.syntax_pass.len(),
        "ignored" => tests.ignored.len());

    for ignored in &tests.ignored {
        let filename = ignored
            .strip_prefix(&test_dir)
            .expect("All tests are inside the test dir");
        debug!(logger, "Ignoring Test"; "filename" => filename.display());
    }

    let logger = root_logger.new(o!("phase" => "testing"));
    let runner = TestRunner::new(&logger);
    let result = runner.execute_test(&tests);

    info!(logger, "Finished Tests";
        "success" => result.is_ok(),
        "parse-successful" => result.syntax_pass_pass.len(),
        "parse-failed" => result.syntax_pass_fail.len());

    let logger = logger.new(o!("phase" => "summary"));

    for (test, outcome) in &result.syntax_pass_fail {
        info!(logger, ""; 
            "test" => test.name(), 
            "error-msg" => outcome.to_string(),
            "full-error" => format!("{:?}", outcome));
    }

    if result.is_ok() {
        0
    } else {
        1
    }
}

fn initialize_logging() -> Logger {
    let drain = slog_term::term_compact().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    Logger::root(drain, o!())
}
