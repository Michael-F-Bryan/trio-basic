# TRIO Basic

[![pipeline status](https://gitlab.com/Michael-F-Bryan/trio-basic/badges/master/pipeline.svg)](https://gitlab.com/Michael-F-Bryan/trio-basic/pipelines)

(**[Rendered Documentation]**)

A proof-of-concept parser and runtime for running *TRIO Basic* programs.

[Rendered Documentation]: https://michael-f-bryan.gitlab.io/trio-basic