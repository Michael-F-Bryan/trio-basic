extern crate codespan;
extern crate codespan_reporting;
extern crate heapsize;
extern crate hir;
extern crate serde;
extern crate serde_json;
extern crate syntax;
extern crate termcolor;

use codespan::{ByteOffset, CodeMap, FileMap};
use heapsize::HeapSizeOf;
use hir::{Diagnostics, Program};
use serde::Serialize;
use syntax::ast::{AstNode, File};
use termcolor::{ColorChoice, StandardStream};

fn run(src: &str, map: &mut CodeMap) -> Result<(), Diagnostics> {
    let filemap = map.add_filemap("dodgy".into(), src.to_string());

    let ast = parse(&filemap)?;

    let program = lower(&ast)?;
    //dump(&program);
    println!("{:?}", program);

    println!("{} bytes used by the HIR", program.heap_size_of_children());

    Ok(())
}

fn main() {
    let mut map = CodeMap::new();
    let src = r#"
    DIM x AS INTEGER
    DIM y AS INTEGER

    start:
    'y = 5
    'x = MAX(3, 4)
    Max(3, 4)
    "#;

    if let Err(diagnostics) = run(src, &mut map) {
        emit_diagnostics(&map, &diagnostics);
    }
}

fn parse(filemap: &FileMap) -> Result<File, Diagnostics> {
    let mut ast = match syntax::parse(filemap.src()) {
        Ok(f) => f,
        Err(mut e) => {
            return Err(Diagnostics::for_parse_error(e, filemap.span()));
        }
    };

    let offset = ByteOffset(filemap.span().start().0 as i64);
    ast.offset_inplace(offset);

    Ok(ast)
}

fn lower(ast: &File) -> Result<Program, Diagnostics> {
    let mut diags = Diagnostics::new();

    match hir::lower(ast, &mut diags) {
        Ok(p) => Ok(p),
        Err(_) => Err(diags),
    }
}

fn emit_diagnostics(map: &CodeMap, diagnostics: &Diagnostics) {
    let mut stream = StandardStream::stdout(ColorChoice::Auto);

    for item in diagnostics.items() {
        codespan_reporting::emit(&mut stream, map, item).unwrap();
    }
}

fn dump<S: Serialize>(obj: &S) {
    let stdout = ::std::io::stdout();

    serde_json::to_writer_pretty(stdout, obj).unwrap();
}
