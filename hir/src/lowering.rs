use codespan_reporting::{Diagnostic, Label};
use diagnostics::Diagnostics;
use ir::{self, BasicBlock, FunctionCall, IrNode, Literal, NodeId, Scope, Variable};
use std::mem;
use syntax::ast::{self, Expr, File, Line};

#[derive(Debug)]
pub struct Lowerer<'diag> {
    next_node_id: u32,
    scope: Scope,
    diagnostics: &'diag mut Diagnostics,
}

impl<'diag> Lowerer<'diag> {
    pub fn new(diagnostics: &'diag mut Diagnostics) -> Self {
        Lowerer {
            next_node_id: 1,
            scope: Default::default(),
            diagnostics: diagnostics,
        }
    }

    pub fn lower(&mut self, ast: &File) -> Result<Scope, ()> {
        debug_assert!(
            self.diagnostics.is_ok(),
            "There was an error before lowering even started"
        );
        // swap in a new Scope
        self.scope = Scope::new(self.next_id());

        self.find_variables(ast);
        self.should_continue()?;

        self.find_labels(ast);
        self.should_continue()?;

        self.resolve_blocks(&ast);
        self.should_continue()?;

        Ok(mem::replace(&mut self.scope, Default::default()))
    }

    /// Convenience method for checking if there was an error and we should bail
    /// early.
    fn should_continue(&self) -> Result<(), ()> {
        if self.diagnostics.is_ok() {
            Ok(())
        } else {
            Err(())
        }
    }

    fn find_variables(&mut self, ast: &File) {
        for line in &ast.lines {
            if let Line::Dim(ref dim) = *line {
                let var = Variable::new(dim.clone(), self.next_id());
                if self.variable_is_valid(&var) {
                    self.scope.insert_variable(var);
                }
            }
        }
    }

    fn find_labels(&mut self, ast: &File) {
        for line in &ast.lines {
            if let Line::Label(ref label) = *line {
                let lbl = ir::Label::new(label.clone(), self.next_id());
                self.scope.insert_label(lbl);
            }
        }
    }

    fn resolve_blocks(&mut self, ast: &File) {
        let mut bb = BasicBlock::new_anonymous(self.next_id());
        self.scope.entry_point = bb.node_id();

        for line in &ast.lines {
            match *line {
                Line::Label(ref label) => {
                    self.resolve_label(label, &mut bb);
                }
                Line::FunctionCall(ref func) => self.resolve_func_call(func, &mut bb),
                Line::Statement(ref stmt) => unimplemented!(),
                Line::Dim(_) => {}
                Line::Assignment(ref ass) => self.resolve_assignment(ass, &mut bb),
            }
        }

        if !bb.is_empty() {
            self.scope.insert_basic_block(bb);
        }
    }

    fn resolve_assignment(&mut self, ass: &ast::Assignment, bb: &mut BasicBlock) {
        unimplemented!()
    }

    fn resolve_label(&mut self, label: &ast::Label, bb: &mut BasicBlock) {
        // create a new BB and link it up to its label and the last BB
        let label_id = self.scope.label_id_by_name(&label.name).unwrap_or_else(|| {
            panic!(
                "We should have already found all labels. This is a bug. Label name: {:?}",
                label.name
            )
        });
        let new_bb = BasicBlock::new(self.next_id(), label_id);
        bb.next = Some(new_bb.node_id());

        self.scope.label_by_id_mut(label_id).unwrap().basic_block = bb.node_id();

        let old_bb = mem::replace(bb, new_bb);
        self.scope.insert_basic_block(old_bb);
    }

    fn resolve_func_call(&mut self, func: &ast::FunctionCall, bb: &mut BasicBlock) {
        let mut args = Vec::new();

        for arg in &func.args {
            let expr_id = self.resolve_expr(arg);
            args.push(expr_id);
        }

        let call = FunctionCall::new(func.name.clone(), args, self.next_id());
        bb.push_instruction(call.node_id());
        self.scope.insert_expr(call);
    }

    fn resolve_expr(&mut self, expr: &Expr) -> NodeId {
        match *expr {
            Expr::Literal(ref lit) => self.resolve_literal(lit),
            _ => unimplemented!(),
        }
    }

    fn resolve_literal(&mut self, lit: &ast::Literal) -> NodeId {
        let expr = Literal::new(lit.clone(), self.next_id());
        let node_id = expr.node_id();
        self.scope.insert_expr(expr);

        node_id
    }

    /// Checks if a variable is valid, automatically emitting the corresponding
    /// diagnostics.
    fn variable_is_valid(&mut self, var: &Variable) -> bool {
        if let Some(original) = self.scope.variable_by_name(&var.name) {
            let diag =
                Diagnostic::new_error(format!("Duplicate variable definition for `{}`", var.name))
                    .with_label(
                        Label::new_primary(var.decl_site.span)
                            .with_message("The duplicate definition was here"),
                    )
                    .with_label(
                        Label::new_secondary(original.decl_site.span)
                            .with_message("But it was originally defined here"),
                    );

            self.diagnostics.add(diag);
            return false;
        }

        true
    }

    fn next_id(&mut self) -> NodeId {
        let n = NodeId::new(self.next_node_id);
        debug_assert!(!n.is_default(), "The placeholder NodeId isn't valid here");
        self.next_node_id += 1;

        n
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use codespan::{ByteIndex, Span};
    use ir::Expression;
    use syntax::ast::{Dim, Ident, Literal};

    #[test]
    fn find_all_variable_declarations() {
        let src = "dim x as integer\ndim y as float";
        let ast = File::from_str(src).unwrap();
        let mut diags = Diagnostics::new();

        let should_be = vec![
            Variable::new(
                Dim {
                    name: Ident::new("x", Span::new(ByteIndex(4), ByteIndex(5))),
                    ty: Ident::new("integer", Span::new(ByteIndex(9), ByteIndex(16))),
                    span: Span::new(ByteIndex(0), ByteIndex(16)),
                },
                NodeId::new(2),
            ),
            Variable::new(
                Dim {
                    name: Ident::new("y", Span::new(ByteIndex(21), ByteIndex(22))),
                    ty: Ident::new("float", Span::new(ByteIndex(26), ByteIndex(31))),
                    span: Span::new(ByteIndex(17), ByteIndex(31)),
                },
                NodeId::new(3),
            ),
        ];

        let got = Lowerer::new(&mut diags).lower(&ast).unwrap();

        assert!(diags.is_ok());

        // check the variable got added to the scope
        for var in &should_be {
            assert_eq!(got.variable_by_name(&var.name).unwrap(), var);
        }
    }

    #[test]
    fn no_duplicate_dims() {
        let ast = File::from_str("dim x as integer\ndim x as float").unwrap();
        let mut diags = Diagnostics::new();

        let got = Lowerer::new(&mut diags).lower(&ast);

        assert!(got.is_err());

        let items = diags.items();
        assert_eq!(items.len(), 1);
        let d = &items[0];
        assert_eq!(d.message, "Duplicate variable definition for `x`");
    }

    #[test]
    fn labels_mark_the_start_of_a_basic_block() {
        let ast = File::from_str("start:\n DIM x as INTEGER\n next:\n foo_bar()\n").unwrap();
        let mut diags = Diagnostics::new();

        let got = Lowerer::new(&mut diags).lower(&ast).unwrap();

        assert!(!diags.is_err());

        // we have the root basic block which is then followed by the "start"
        // block and the "next" block
        assert_eq!(1 + 2, got.basic_blocks.len());

        let root_bb = got.entry_point;
        assert_ne!(
            root_bb,
            NodeId::default(),
            "The root bb should have been set"
        );
        let root = &got.basic_blocks[&root_bb];

        // we can also get to the "next" block by following the chain
        let start_id = root.next.unwrap();
        let start = &got.basic_blocks[&start_id];
        let start_label_id = start.label.unwrap();
        let start_name = got.label_by_id(start_label_id).unwrap().name();
        assert_eq!(start_name, "start");

        let next_id = start.next.unwrap();
        let next = &got.basic_blocks[&next_id];
        let next_label_id = next.label.unwrap();
        let next_name = got.label_by_id(next_label_id).unwrap().name();
        assert_eq!(next_name, "next");
    }

    #[test]
    fn resolve_a_literal() {
        let mut diags = Diagnostics::new();
        let expr = Expr::Literal(Literal::new(3.14, Default::default()));
        let expr_id: NodeId;

        let scope = {
            let mut l = Lowerer::new(&mut diags);
            expr_id = l.resolve_expr(&expr);
            l.scope
        };

        assert!(diags.is_ok());

        assert_eq!(scope.expressions.len(), 1);

        match scope.expressions[&expr_id] {
            Expression::Literal(ref lit) => {
                let got = Expr::Literal(lit.inner.clone());
                assert_eq!(got, expr);
            }
            ref got => unreachable!("Should never get {:?}", got),
        }
    }

    #[test]
    fn resolve_a_function_call() {
        let mut diags = Diagnostics::new();
        let ast = File::from_str("foo(3.14, 5)").unwrap();

        let got = Lowerer::new(&mut diags).lower(&ast).unwrap();

        assert!(diags.is_ok());

        assert_eq!(got.expressions.len(), 3);
        assert_eq!(got.basic_blocks.len(), 1);
    }
}
