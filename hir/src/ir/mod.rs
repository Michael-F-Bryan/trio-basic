use syntax::ast::{self, Dim, Ident};

mod basic_block;
mod ir_node;
mod scope;

pub use self::basic_block::BasicBlock;
pub use self::ir_node::{IrNode, NodeId};
pub use self::scope::Scope;

/// All the IR for a single executable TRIO Basic program.
#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
#[serde(rename_all = "kebab-case")]
pub struct Program {
    pub top_level_scope: Scope,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
#[serde(rename_all = "kebab-case")]
pub struct Variable {
    node_id: NodeId,
    pub name: String,
    pub decl_site: Dim,
}

impl Variable {
    pub fn new(decl_site: Dim, node_id: NodeId) -> Variable {
        let name = decl_site.name.name.clone();

        Variable {
            node_id,
            name,
            decl_site,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
pub enum Expression {
    FunctionCall(FunctionCall),
    Literal(Literal),
}

impl IrNode for Expression {
    fn node_id(&self) -> NodeId {
        match *self {
            Expression::FunctionCall(ref f) => f.node_id(),
            Expression::Literal(ref lit) => lit.node_id(),
        }
    }
}

impl From<FunctionCall> for Expression {
    fn from(other: FunctionCall) -> Expression {
        Expression::FunctionCall(other)
    }
}

impl From<Literal> for Expression {
    fn from(other: Literal) -> Expression {
        Expression::Literal(other)
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
pub struct FunctionCall {
    node_id: NodeId,
    pub args: Vec<NodeId>,
    pub name: Ident,
}

impl FunctionCall {
    pub fn new(name: Ident, args: Vec<NodeId>, node_id: NodeId) -> FunctionCall {
        FunctionCall {
            name,
            args,
            node_id,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
pub struct Literal {
    node_id: NodeId,
    pub inner: ast::Literal,
}

impl Literal {
    pub fn new(lit: ast::Literal, node_id: NodeId) -> Literal {
        Literal {
            inner: lit,
            node_id,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
#[serde(rename_all = "kebab-case")]
pub struct Label {
    node_id: NodeId,
    pub decl: ast::Label,
    pub basic_block: NodeId,
}

impl Label {
    pub fn new(decl: ast::Label, node_id: NodeId) -> Label {
        Label {
            decl,
            node_id,
            // This will be filled in later
            basic_block: NodeId::default(),
        }
    }

    pub fn name(&self) -> &str {
        &self.decl.name
    }
}

impl_ir_node!(Variable);
impl_ir_node!(Literal);
impl_ir_node!(FunctionCall);
impl_ir_node!(Label);
