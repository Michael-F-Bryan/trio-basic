use ir::{BasicBlock, Expression, IrNode, Label, NodeId, Variable};
use std::collections::HashMap;
use std::fmt::Debug;

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
#[serde(rename_all = "kebab-case")]
pub struct Scope {
    node_id: NodeId,
    /// All the variables available in the current scope.
    pub variables: HashMap<NodeId, Variable>,
    /// A convenience table for looking up a node given its `NodeId`.
    pub variable_names: HashMap<String, NodeId>,
    pub basic_blocks: HashMap<NodeId, BasicBlock>,
    pub labels: HashMap<NodeId, Label>,
    pub label_names: HashMap<String, NodeId>,
    pub expressions: HashMap<NodeId, Expression>,
    /// The `NodeId` of the first basic block in this scope.
    pub entry_point: NodeId,
}

impl Scope {
    pub fn new(node_id: NodeId) -> Scope {
        Scope {
            node_id,
            ..Default::default()
        }
    }

    pub fn insert_expr<E: Into<Expression>>(&mut self, expr: E) {
        let expr = expr.into();
        self.duplicate_check(&expr);
        self.expressions.insert(expr.node_id(), expr);
    }

    pub fn insert_basic_block(&mut self, bb: BasicBlock) {
        self.duplicate_check(&bb);
        self.basic_blocks.insert(bb.node_id(), bb);
    }

    /// A debug-only check to make sure we aren't accidentally trying to add
    /// things twice or two nodes with the same `NodeId`.
    fn duplicate_check<I: IrNode + Debug>(&self, item: &I) {
        if cfg!(debug_assertions) {
            let id = item.node_id();

            assert_ne!(
                id,
                NodeId::default(),
                "An item's NodeId should never be the placeholder NodeId"
            );

            if self.lookup(id).is_some() {
                panic!(
                    "Attempting to insert a duplicate node with {:?}. This Is A Bug.\nItem: {:#?}",
                    id, item
                );
            }
        }
    }

    pub fn insert_label(&mut self, lbl: Label) {
        self.duplicate_check(&lbl);

        let node_id = lbl.node_id;
        let name = lbl.decl.name.to_lowercase();

        self.labels.insert(node_id, lbl);
        self.label_names.insert(name, node_id);
    }

    pub fn insert_variable(&mut self, var: Variable) {
        self.duplicate_check(&var);

        let node_id = var.node_id;
        let name = var.name.to_lowercase();

        self.variables.insert(node_id, var);
        self.variable_names.insert(name, node_id);
    }

    pub fn variable_id_by_name(&self, name: &str) -> Option<NodeId> {
        let name = name.to_lowercase();
        self.variable_names.get(&name).cloned()
    }

    pub fn variable_by_id(&self, node_id: NodeId) -> Option<&Variable> {
        self.variables.get(&node_id)
    }

    pub fn variable_by_name(&self, name: &str) -> Option<&Variable> {
        let id = self.variable_id_by_name(name)?;
        self.variable_by_id(id)
    }

    pub fn label_id_by_name(&self, name: &str) -> Option<NodeId> {
        let name = name.to_lowercase();
        self.label_names.get(&name).cloned()
    }

    pub fn label_by_id(&self, node_id: NodeId) -> Option<&Label> {
        self.labels.get(&node_id)
    }

    pub fn label_by_id_mut(&mut self, node_id: NodeId) -> Option<&mut Label> {
        self.labels.get_mut(&node_id)
    }

    pub fn label_by_name(&self, name: &str) -> Option<&Label> {
        let node_id = self.label_id_by_name(name)?;
        self.label_by_id(node_id)
    }

    /// Get a dynamically typed item by its `NodeId`.
    pub fn lookup(&self, node_id: NodeId) -> Option<&IrNode> {
        if let Some(var) = self.variables.get(&node_id) {
            return Some(var);
        }

        if let Some(lbl) = self.labels.get(&node_id) {
            return Some(lbl);
        }

        if let Some(bb) = self.basic_blocks.get(&node_id) {
            return Some(bb);
        }

        if let Some(expr) = self.expressions.get(&node_id) {
            return Some(expr);
        }

        None
    }

    /// Iterate over all `IrNode`s in this `Scope`.
    pub fn all_items(&self) -> impl Iterator<Item = &IrNode> {
        let vars = self.variables.values().map(|v| v as &IrNode);
        let lbls = self.labels.values().map(|v| v as &IrNode);
        let bbs = self.basic_blocks.values().map(|v| v as &IrNode);
        let exprs = self.expressions.values().map(|v| v as &IrNode);

        vars.chain(lbls).chain(bbs).chain(exprs)
    }
}

impl_ir_node!(Scope);

#[cfg(test)]
mod tests {
    use super::*;
    use codespan::Span;
    use syntax::ast::{Dim, Ident};

    #[test]
    fn inserting_a_variable_also_updates_the_names_table() {
        let mut s = Scope::default();

        let node_id = NodeId::new(1);
        let var = Variable::new(
            Dim::new(
                Ident::new("x", Span::default()),
                Ident::new("integer", Span::default()),
                Span::default(),
            ),
            node_id,
        );

        s.insert_variable(var.clone());

        assert_eq!(s.variables.len(), 1);
        assert_eq!(s.variables[&node_id], var);
        assert_eq!(s.variable_names["x"], node_id);

        // lookups should also be case insensitive
        assert_eq!(s.variable_by_name("x").unwrap(), &var);
        assert_eq!(s.variable_by_name("X").unwrap(), &var);
    }
}
