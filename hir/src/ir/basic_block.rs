use ir::NodeId;

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize, HeapSizeOf)]
#[serde(rename_all = "kebab-case")]
pub struct BasicBlock {
    node_id: NodeId,
    /// A pointer to the label which defines this basic block, if there is one.
    pub label: Option<NodeId>,
    pub instructions: Vec<NodeId>,
    pub next: Option<NodeId>,
}

impl BasicBlock {
    pub fn new(node_id: NodeId, label_id: NodeId) -> BasicBlock {
        BasicBlock {
            node_id,
            label: Some(label_id),
            ..Default::default()
        }
    }

    pub fn new_anonymous(node_id: NodeId) -> BasicBlock {
        BasicBlock {
            node_id,
            ..Default::default()
        }
    }

    pub fn push_instruction(&mut self, node_id: NodeId) {
        self.instructions.push(node_id);
    }

    pub fn is_empty(&self) -> bool {
        self.instructions.is_empty()
    }
}

impl_ir_node!(BasicBlock);
