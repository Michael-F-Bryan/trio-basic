use std::any::Any;
use std::fmt::Debug;

pub trait IrNode: Any + Debug {
    fn node_id(&self) -> NodeId;
}

#[derive(Debug, Default, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize, HeapSizeOf)]
pub struct NodeId(u32);

impl NodeId {
    pub(crate) fn new(n: u32) -> NodeId {
        NodeId(n)
    }

    /// The `NodeId` returned by `Default` is often used as a placeholder for
    /// something that needs its own `NodeId` but it hasn't been computed for
    /// yet.
    pub fn is_default(&self) -> bool {
        *self == NodeId::default()
    }
}
