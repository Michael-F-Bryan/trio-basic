//! The high-level intermediate representation for TRIO Basic code.
//!
//! The high-level IR is essentially just a combination of basic blocks and
//! three-address codes.

extern crate codespan;
extern crate codespan_reporting;
extern crate heapsize;
#[macro_use]
extern crate heapsize_derive;
extern crate serde;
extern crate syntax;
#[macro_use]
extern crate serde_derive;

#[cfg(test)]
extern crate termcolor;
#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

#[macro_use]
mod macros;
pub mod diagnostics;
pub mod ir;
mod lowering;

pub use diagnostics::Diagnostics;
pub use ir::Program;

use lowering::Lowerer;
use syntax::ast::File;

pub fn lower(ast: &File, diags: &mut Diagnostics) -> Result<Program, ()> {
    let scope = Lowerer::new(diags).lower(ast)?;

    if diags.is_ok() {
        Ok(Program {
            top_level_scope: scope,
        })
    } else {
        Err(())
    }
}
