use codespan::{ByteIndex, ByteOffset, Span};
use codespan_reporting::{Diagnostic, Label, Severity};
use syntax::ParseError;

#[derive(Debug, Default, Clone)]
pub struct Diagnostics {
    items: Vec<Diagnostic>,
}

impl Diagnostics {
    pub fn new() -> Diagnostics {
        Default::default()
    }

    /// Generate a new set of `Diagnostics` for the provided
    /// `syntax::ParseError`, offsetting the location so it is within the
    /// filemap.
    pub fn for_parse_error(mut err: ParseError, filemap_span: Span<ByteIndex>) -> Diagnostics {
        err.offset_inplace(ByteOffset(filemap_span.start().0 as i64));

        let span = err
            .span()
            .unwrap_or_else(|| filemap_span.with_start(filemap_span.end() - ByteOffset(1)));

        let diag = Diagnostic::new_error(err.to_string()).with_label(Label::new_primary(span));

        Diagnostics { items: vec![diag] }
    }

    pub fn items(&self) -> &[Diagnostic] {
        &self.items
    }

    pub fn is_ok(&self) -> bool {
        !self.is_err()
    }

    pub fn is_err(&self) -> bool {
        self.items.iter().any(|it| it.severity > Severity::Warning)
    }

    pub fn add(&mut self, diag: Diagnostic) {
        self.items.push(diag);
    }
}
