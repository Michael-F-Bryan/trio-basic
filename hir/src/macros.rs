macro_rules! impl_ir_node {
    ($type:ident) => {
        impl $crate::ir::IrNode for $type {
            fn node_id(&self) -> $crate::ir::NodeId {
                self.node_id
            }
        }
    };
}