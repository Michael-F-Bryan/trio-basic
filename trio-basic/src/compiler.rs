use codespan::{CodeMap, FileMap};
use codespan_reporting;
use codespan_reporting::termcolor::{ColorChoice, StandardStream};
use heapsize::HeapSizeOf;
use hir;
use hir::{Diagnostics, Program};
use outcome::Outcome;
use runtime::VirtualMachine;
use slog::Logger;
use std::path::Path;
use std::sync::Arc;
use syntax;
use syntax::ast::File;
use Colour;

pub struct Compiler {
    codemap: CodeMap,
    diags: Diagnostics,
    logger: Logger,
    colour_choice: ColorChoice,
}

impl Compiler {
    pub fn new(logger: &Logger, colour: Colour) -> Compiler {
        let colour_choice = if colour.is_coloured() {
            ColorChoice::Always
        } else {
            ColorChoice::Never
        };

        Compiler {
            codemap: CodeMap::new(),
            diags: Diagnostics::new(),
            logger: logger.clone(),
            colour_choice,
        }
    }

    pub fn run(&mut self, filename: &Path) -> Outcome {
        let filemap = try!(self.load_filemap(filename));
        let ast = try!(self.parse(&filemap));
        let program = try!(self.lower(&ast));
        try!(self.execute_program(&program));

        Outcome::Ok(())
    }

    fn execute_program(&mut self, program: &Program) -> Outcome {
        debug!(self.logger, "Executing the program");
        let mut vm = VirtualMachine::new_with_logger(&self.logger);

        try!(vm.exec_program(program));

        Outcome::Ok(())
    }

    fn lower(&mut self, ast: &File) -> Outcome<Program> {
        debug!(self.logger, "Lowering to internal representation");

        match hir::lower(ast, &mut self.diags) {
            Ok(prog) => {
                debug!(self.logger, "Lowering was successful";
                    "memory-used" => prog.heap_size_of_children());

                Outcome::Ok(prog)
            }
            Err(_) => {
                try!(self.emit_diagnostics(&self.diags));
                Outcome::ExpectedFailure
            }
        }
    }

    fn parse(&self, filemap: &FileMap) -> Outcome<File> {
        debug!(self.logger, "Parsing input to AST"; 
            "length" => filemap.src().len());

        match syntax::parse_from_filemap(filemap) {
            Ok(f) => {
                debug!(self.logger, "Parse successful";
                    "memory-used" => f.heap_size_of_children());

                Outcome::Ok(f)
            }
            Err(e) => {
                let diags = Diagnostics::for_parse_error(e, filemap.span());
                self.emit_diagnostics(&diags);

                Outcome::ExpectedFailure
            }
        }
    }

    fn load_filemap(&mut self, filename: &Path) -> Outcome<Arc<FileMap>> {
        debug!(self.logger, "Loading input file"; 
            "filename" => filename.display());

        match self.codemap.add_filemap_from_disk(filename) {
            Ok(f) => Outcome::Ok(f),
            Err(e) => {
                error!(self.logger, "Unable to load the input file";
                    "error" => e.to_string());

                Outcome::ExpectedFailure
            }
        }
    }

    fn emit_diagnostics(&self, diagnostics: &Diagnostics) -> Outcome {
        let mut stream = StandardStream::stdout(self.colour_choice);

        for item in diagnostics.items() {
            try!(codespan_reporting::emit(&mut stream, &self.codemap, item));
        }

        Outcome::Ok(())
    }
}
