use failure::Error;

macro_rules! try {
    ($outcome:expr) => {
        match $outcome.into() {
            $crate::Outcome::Ok(value) => value,
            $crate::Outcome::InternalCompilerError(e) => {
                return $crate::Outcome::InternalCompilerError(e)
            }
            $crate::Outcome::ExpectedFailure => return $crate::Outcome::ExpectedFailure,
        }
    };
}

#[derive(Debug)]
pub enum Outcome<V = ()> {
    Ok(V),
    InternalCompilerError(Error),
    ExpectedFailure,
}

impl Outcome {
    pub fn is_ok(&self) -> bool {
        match *self {
            Outcome::Ok(_) => true,
            _ => false,
        }
    }
}

impl<V, E: Into<Error>> From<Result<V, E>> for Outcome<V> {
    fn from(other: Result<V, E>) -> Outcome<V> {
        match other {
            Ok(v) => Outcome::Ok(v),
            Err(e) => Outcome::InternalCompilerError(e.into()),
        }
    }
}
