#[macro_use]
extern crate failure;
extern crate atty;
extern crate runtime;
#[macro_use]
extern crate structopt;
#[macro_use]
extern crate slog;
extern crate codespan;
extern crate codespan_reporting;
extern crate heapsize;
extern crate hir;
extern crate slog_async;
extern crate slog_term;
extern crate syntax;

#[macro_use]
mod outcome;
mod compiler;

pub use compiler::Compiler;
pub use outcome::Outcome;

use failure::Error;
use slog::{Drain, Level, Logger, Record, Serializer, Value, KV};
use std::path::PathBuf;
use std::process;
use std::str::FromStr;
use structopt::StructOpt;

fn run() -> i32 {
    let args = Args::from_args();
    let logger = initialize_logging(args.verbosity, args.colour);

    let mut compiler = Compiler::new(&logger, args.colour);
    let outcome = compiler.run(&args.input);

    drop(logger);
    drop(compiler);

    if let Outcome::InternalCompilerError(ref e) = outcome {
        eprintln!("Error: {}", e);

        for cause in e.causes().skip(1) {
            eprintln!("\tCaused By: {}", cause);
        }

        let bt = e.backtrace().to_string();
        if !bt.trim().is_empty() {
            eprintln!("{}", bt);
        }

        1
    } else {
        0
    }
}

fn main() {
    // We can't have process::exit() in the run() function because it won't
    // properly drop (and flush) our logger.
    process::exit(run());
}

#[derive(Debug, Clone, PartialEq, StructOpt)]
struct Args {
    #[structopt(
        short = "v", long = "verbose", help = "Enable verbose output", parse(from_occurrences)
    )]
    verbosity: u64,
    #[structopt(help = "The program to execute", parse(from_os_str))]
    input: PathBuf,
    #[structopt(
        long = "colour",
        help = "Output colouring",
        default_value = "auto",
        raw(possible_values = r#"&["auto", "always", "never"]"#)
    )]
    colour: Colour,
}

impl KV for Args {
    fn serialize(&self, record: &Record, ser: &mut Serializer) -> slog::Result {
        let Args {
            verbosity,
            ref input,
            colour,
        } = *self;

        ser.emit_u64("verbosity", verbosity)?;
        input.display().serialize(record, "input", ser)?;
        ser.emit_arguments("colour", &format_args!("{:?}", colour))?;

        Ok(())
    }
}

fn initialize_logging(verbosity: u64, colour: Colour) -> Logger {
    let level = match verbosity {
        0 => Level::Warning,
        1 => Level::Info,
        2 => Level::Debug,
        _ => Level::Trace,
    };

    let async = if colour.is_coloured() {
        let drain = slog_term::term_full().fuse();
        slog_async::Async::new(drain).build().fuse()
    } else {
        let decorator = slog_term::PlainDecorator::new(::std::io::stdout());
        let drain = slog_term::FullFormat::new(decorator).build().fuse();
        slog_async::Async::new(drain).build().fuse()
    };

    let drain = async.filter_level(level).fuse();

    Logger::root(drain, o!())
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Colour {
    Auto,
    Always,
    Never,
}

impl Colour {
    pub fn is_coloured(&self) -> bool {
        match *self {
            Colour::Always => true,
            Colour::Never => false,
            Colour::Auto => atty::is(atty::Stream::Stdout),
        }
    }
}

impl FromStr for Colour {
    type Err = Error;

    fn from_str(s: &str) -> Result<Colour, Self::Err> {
        match s.to_lowercase().as_str() {
            "auto" => Ok(Colour::Auto),
            "always" => Ok(Colour::Always),
            "never" => Ok(Colour::Never),
            _ => Err(format_err!("Invalid colour choice, {:?}", s)),
        }
    }
}
