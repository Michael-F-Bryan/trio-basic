extern crate runtime;

use runtime::VirtualMachine;

#[test]
#[ignore]
fn execute_a_snippet_of_code() {
    let mut vm = VirtualMachine::new();
    let src = "TABLE(5, 3.14)";

    vm.set_table(5, 0.0);
    vm.exec(src).unwrap();
    assert_eq!(vm.get_table(5), 3.14);
}
