extern crate codespan;
extern crate failure;
#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate slog;
extern crate heapsize;
extern crate hir;
extern crate serde;
extern crate syntax;
#[macro_use]
extern crate serde_derive;

pub mod event_handler;
pub mod types;
mod vm;

pub use vm::VirtualMachine;
