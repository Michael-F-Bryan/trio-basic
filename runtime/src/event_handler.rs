use std::rc::Rc;
use std::sync::Arc;

pub type Thunk = EventHandler<()>;

/// A generic event handler which may be used as a callback.
pub trait EventHandler<A: ?Sized> {
    fn invoke(&self, args: &A);
}

impl<A: ?Sized, E: EventHandler<A> + ?Sized> EventHandler<A> for Arc<E> {
    fn invoke(&self, args: &A) {
        (&**self).invoke(args);
    }
}

impl<A: ?Sized, E: EventHandler<A> + ?Sized> EventHandler<A> for Rc<E> {
    fn invoke(&self, args: &A) {
        (&**self).invoke(args);
    }
}

impl<A, F> EventHandler<A> for F
where
    A: ?Sized,
    F: Fn(&A),
{
    fn invoke(&self, args: &A) {
        self(args);
    }
}

pub struct EventList<A: ?Sized> {
    handlers: Vec<Arc<EventHandler<A>>>,
}

impl<A: ?Sized> EventList<A> {
    pub fn new() -> Self {
        EventList::default()
    }

    pub fn register(&mut self, handler: Arc<EventHandler<A>>) {
        self.handlers.push(handler);
    }

    pub fn unregister(&mut self, handler: &Arc<EventHandler<A>>) {
        self.handlers.retain(|h| !Arc::ptr_eq(h, handler));
    }

    pub fn len(&self) -> usize {
        self.handlers.len()
    }

    pub fn is_empty(&self) -> bool {
        self.handlers.is_empty()
    }
}

impl<A: ?Sized> Default for EventList<A> {
    fn default() -> Self {
        EventList {
            handlers: Vec::new(),
        }
    }
}

impl<A: ?Sized> EventHandler<A> for EventList<A> {
    fn invoke(&self, args: &A) {
        self.handlers
            .iter()
            .for_each(|handler| handler.invoke(args));
    }
}
