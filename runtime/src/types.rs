use failure::Error;
use vm::VirtualMachine;

pub trait Function {
    fn signature(&self) -> &FunctionSignature;
    fn call(&self, args: &[Value], vm: &mut VirtualMachine) -> Result<Option<Value>, Error>;
}

#[derive(Debug, Clone, PartialEq)]
pub struct FunctionSignature {
    pub args: Vec<Type>,
    pub ret: Option<Type>,
    pub varargs: bool,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Type {
    Nil,
    Float,
    Integer,
    String,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Value {
    Nil,
    Float(f64),
    Integer(i64),
    String(String),
}

impl Value {
    pub fn kind(&self) -> Type {
        match *self {
            Value::Nil => Type::Nil,
            Value::Float(_) => Type::Float,
            Value::Integer(_) => Type::Integer,
            Value::String(_) => Type::String,
        }
    }
}
