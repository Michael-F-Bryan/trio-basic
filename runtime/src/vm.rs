use event_handler::{EventHandler, EventList};
use failure::Error;
use heapsize::HeapSizeOf;
use hir::{self, Diagnostics, Program};
use slog::{Discard, Logger};
use std::fmt::{self, Display, Formatter};
use syntax::ast::File;

#[derive(Serialize, Deserialize)]
pub struct VirtualMachine {
    table: Vec<f64>,
    #[serde(skip, default = "discarding_logger")]
    logger: Logger,
    #[serde(skip, default)]
    before_exec: EventList<str>,
    #[serde(skip, default)]
    after_parse: EventList<File>,
    #[serde(skip, default)]
    after_lowering: EventList<Program>,
}

impl VirtualMachine {
    pub const DEFAULT_TABLE_SIZE: usize = 512_000;

    pub fn new() -> VirtualMachine {
        VirtualMachine::new_with_logger(&discarding_logger())
    }

    pub fn new_with_logger(logger: &Logger) -> VirtualMachine {
        VirtualMachine {
            logger: logger.clone(),
            ..Default::default()
        }
    }

    pub fn get_table(&self, key: usize) -> f64 {
        let value = self.table[key];
        trace!(self.logger, "Fetching table value";
            "key" => key,
            "value" => value);

        value
    }

    pub fn set_table(&mut self, key: usize, value: f64) {
        trace!(self.logger, "Setting a table value";
            "key" => key,
            "value" => value);

        self.table[key] = value;
    }

    pub fn exec(&mut self, src: &str) -> Result<(), Error> {
        debug!(self.logger, "Executing source code";
            "length" => src.len());
        trace!(self.logger, "code"; "src" => &src);
        self.before_exec.invoke(src);

        let f = File::from_str(src)?;

        debug!(self.logger, "Source code parsed";
            "memory-used" => f.heap_size_of_children());
        self.after_parse.invoke(&f);

        let mut diags = Diagnostics::new();
        let program = match hir::lower(&f, &mut diags) {
            Ok(p) => p,
            Err(_) => return Err(LoweringFailed(diags).into()),
        };
        debug!(self.logger, "Converted to IR";
            "memory-used" => program.heap_size_of_children(),
            "num-elements" => program.top_level_scope.all_items().count());
        self.after_lowering.invoke(&program);

        self.exec_program(&program)
    }

    pub fn exec_program(&mut self, program: &Program) -> Result<(), Error> {
        unimplemented!()
    }
}

impl Default for VirtualMachine {
    fn default() -> VirtualMachine {
        VirtualMachine {
            table: vec![0.0; Self::DEFAULT_TABLE_SIZE],
            logger: discarding_logger(),
            before_exec: EventList::new(),
            after_parse: EventList::new(),
            after_lowering: EventList::new(),
        }
    }
}

fn discarding_logger() -> Logger {
    Logger::root(Discard, o!())
}

#[derive(Debug, Fail)]
pub struct LoweringFailed(pub Diagnostics);

impl Display for LoweringFailed {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "Encountered {} issues while parsing",
            self.0.items().len()
        )
    }
}
